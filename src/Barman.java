import java.util.ArrayList;

public class Barman extends Thread {

	private String nomCocktail;
	private ArrayList<Ingredient> listeIngredients;
	
	
	public Barman (String pNomCocktail) {
		this.nomCocktail = pNomCocktail;
		
//		this.run();
	}
	
	// synchronized évite l'interruption par un autre thread
	public void preparerCocktail(ArrayList<Ingredient> listeIngredients) throws InterruptedException 
	{
		this.listeIngredients = listeIngredients;
		System.out.println("Préparation d'un cocktail : " + nomCocktail);
		for (Ingredient ingredient : listeIngredients) {
			ingredient.prendre();
		};
		
		
	}
	
	public void run() {
		try {
			Thread.sleep(5000);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void finirCocktail() throws InterruptedException 
	{
		for (Ingredient ingredient : listeIngredients) {
			ingredient.poser();
		};
		
		System.out.println("Préparation du cocktail terminé");
	}


}
