import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		boolean is_running = true;
		
		Barman cedric = new Barman("Tequila Sunrise");
		Barman marine = new Barman("Mojito");
		
		Ingredient menthe = new Ingredient("Menthe");
		Ingredient glacepil = new Ingredient("Glace pil�e");
		Ingredient limonade= new Ingredient("Limonade");
		Ingredient citronv= new Ingredient("Citron vert");
		Ingredient orange = new Ingredient("Orange");
		Ingredient grenadine = new Ingredient("Grenadine");
		
		ArrayList<Ingredient> listeCedric = new ArrayList<>();
		listeCedric.add(menthe);
		listeCedric.add(glacepil);
		listeCedric.add(limonade);
		listeCedric.add(citronv);
		ArrayList<Ingredient> listeMarine = new ArrayList<>();
		listeMarine.add(grenadine);
		listeMarine.add(orange);
		listeMarine.add(glacepil);
		listeMarine.add(menthe);

		cedric.start();
		marine.start();
		
		while(is_running == true) {
			cedric.preparerCocktail(listeCedric);
			cedric.sleep(2000);
			cedric.finirCocktail();
			
			marine.preparerCocktail(listeMarine);
			marine.sleep(2000);
			marine.finirCocktail();	
		}
		
		cedric.join();
		marine.join();
		
		
		
	}

}
