
public class old extends Thread
{
  private int sem = 1;
  private int intervalle, limite;
  private String nomCocktail;

  public old (String nomCocktail) {
	  this.nomCocktail = nomCocktail;  
  }
  
  public void preparerCocktail() throws InterruptedException {
		System.out.println("Pr�paration d'un cocktail :" + nomCocktail);
		if(sem == 0) {
			this.wait();// attente ind�finie d'une notigication par le thread qui poss�de la ressource
		}
		sem = sem - 1;
  }
  
  public void finirCocktail() {
		sem = sem + 1;
		notify(); // R�veille le premier thread bloqu� sur wait
		System.out.println("Cocktail " + nomCocktail + " termin�");
  }
  
  public void Compteur(int intervalle, int limite) 
  {
    this.intervalle = intervalle;
    this.limite = limite;
  }
  @Override
  public void run()
  {
    try 
    {
      for(int i=0; i<limite; i+=intervalle) {
        System.out.printf("%d: %d\n", this.hashCode(), i);
        Thread.sleep(10);
      }  
    }
    catch(InterruptedException e)
    {
      System.err.println(e.getMessage());
    }
  }
}
