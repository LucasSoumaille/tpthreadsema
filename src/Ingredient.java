import java.util.concurrent.TimeUnit;

public class Ingredient {
	
	String nom;
	private int sem = 1;
	
	public Ingredient(String pNom) {
		this.nom = pNom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public synchronized void prendre() throws InterruptedException {
		System.out.println("Tentative de l'ingr�dient : " + nom);
		System.out.println("AVANT");
		TimeUnit.SECONDS.sleep(1);
		System.out.println("APRES");
		if(sem == 0) {
			wait();// attente ind�finie d'une notigication par le thread qui poss�de la ressource
		}
		System.out.println("Prise de l'ingr�dient : " + nom);
		sem = sem - 1;
  }
	
	public synchronized void poser() {
		sem = sem + 1;
		notify(); // R�veille le premier thread bloqu� sur wait
		System.out.println("Pose de l'ingr�dient: " + nom);
  }
}
